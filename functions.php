<?php
/**
 * Electro - ShopOfThings
 *
 * @package electro-shopofthings
 */
add_action('wp_enqueue_scripts', 'electro_shopofthings_enqueue_styles');
function electro_shopofthings_enqueue_styles() {
    $parent_style = 'electro';
    wp_enqueue_style($parent_style, get_template_directory_uri() . '/style.css');
    wp_enqueue_style('electro-shopofthings', get_stylesheet_directory_uri() . '/style.css', array($parent_style), wp_get_theme()->get('Version'));
}



require_once '/var/www/vhosts/jan/shopofthings/wordpress/wp-content/geoip/vendor/autoload.php';
use GeoIp2\Database\Reader;
function createAdsenseBlogResponsive() {
    $ad = '';

    // This creates the Reader object, which should be reused across
    // lookups.
    $reader = new Reader('/var/www/vhosts/jan/shopofthings/wordpress/wp-content/geoip/GeoLite2-Country_20190205/GeoLite2-Country.mmdb');
    
    
    if (!isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $IP = $_SERVER['REMOTE_ADDR'];
    }
    else {
        $IP = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    $record = $reader->country($IP);
    $ISO = $record->country->isoCode;

    if (!in_array($ISO, array('CH', 'LI'))) {
        $ad = '<div align="center">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Blog_Ad -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-9031640881990657"
     data-ad-slot="1335518191"
     data-ad-format="auto"
     data-full-width-responsive="true"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script></div>
             ';
    }
    return $ad;
}
add_shortcode('adsenseBlogResponsive', 'createAdsenseBlogResponsive');

?>